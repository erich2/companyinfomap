#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

typedef struct
{
    char    *symbol;
    char    *name;
    float    lastSale;
    float    marketCap;
    int      IPOyear;
} Company;

void PrintCompany(Company *c)
{
    printf("%s:\n", c->name);
    printf("\tSymbol: %s\n", c->symbol);
    /* .2f: only print two digits after the period. */
    printf("\tLast Sale: %.2f\n", c->lastSale);
    printf("\tMarket Capitalization: %.2f\n", c->marketCap);
    printf("\tYear of Initial Public Offering: %d\n", c->IPOyear);
}

struct timeval tv1, tv2, tv3, tv4;


void ReadFile(const char *filename, Company **companies_rv, int *numCompanies_rv)
{
    int  i, j;

    if (filename == NULL)
    {
        fprintf(stderr, "No filename specified!\n");
        exit(EXIT_FAILURE);
    }
    FILE *f_in = fopen(filename, "r");
    if (f_in == NULL)
    {
        fprintf(stderr, "Unable to open file \"%s\"\n", filename);
        exit(EXIT_FAILURE);
    }

    fseek(f_in, 0, SEEK_END);
    int numChars = ftell(f_in);
    // printf("The number of characters is %d\n", numChars);
    fseek(f_in, 0, SEEK_SET);

    char *file_contents = malloc(sizeof(char)*numChars+1);
    fread(file_contents, sizeof(char), numChars, f_in);
    file_contents[numChars] = '\0';
    fclose(f_in);
    /* Note: the memory for this array is used to populate
     * the fields of the companies.  If it is freed, then
     * the company structs all become invalid.  For the
     * context of this program, this array should not be 
     * freed. */

    // Find out how many lines there are
    int numLines = 0;
    for (i = 0 ; i < numChars ; i++)
        if (file_contents[i] == '\n')
            numLines++;
    // printf("Number of lines is %d\n", numLines);

    int      numCompanies = numLines-1; // first line is header info 
    Company *companies    = malloc(sizeof(Company)*numCompanies);

    /* strtok will parse the file_contents array.  
     * The first time we call it, it will replace every '"' with '\0'.
     * It will also return the first word before a 
     */
    int numColumns = 9;
    int numberOfQuotesPerColumn = 2;
    strtok(file_contents, "\"");
    for (i = 0 ; i < numberOfQuotesPerColumn*numColumns-1 ; i++)
         strtok(NULL, "\"");
    for (i = 0 ; i < numCompanies ; i++)
    {
         companies[i].symbol = strtok(NULL, "\"");
         strtok(NULL, "\"");
         companies[i].name = strtok(NULL, "\"");
         strtok(NULL, "\"");
         companies[i].lastSale = atof(strtok(NULL, "\""));
         strtok(NULL, "\"");
         companies[i].marketCap = atof(strtok(NULL, "\""));
         strtok(NULL, "\""); 

         /* Skip ADR TSO */
         strtok(NULL, "\"");
         strtok(NULL, "\"");

         companies[i].IPOyear = atoi(strtok(NULL, "\""));
         strtok(NULL, "\"");

         /* Skip Sector, Industry, Summary Quote */
         for (j = 0 ; j < 6 ; j++)
             strtok(NULL, "\"");

         //PrintCompany(companies+i);
    }

    /* Set parameters to have output values */
    *companies_rv    = companies;
    *numCompanies_rv = numCompanies;
}

int DoubleHash(int key){
	int prime = 7;
	return (prime - (key % prime));
}


int hash(char *name, long array_size)
{
    int hashval = 0;
    char *p = name;
    while (*p != '\0')
    {
        hashval = 31*hashval + *p;
        p++;
    }

    return hashval%array_size;
}

typedef struct 
{
    int         numElements;
    char      **keys;
    Company    *companies;
} MapBasedOnHashTable;

struct MapBasedOnLinkedList {
	char *key;
	Company *company;	
	struct MapBasedOnLinkedList *next;
};

struct MapBasedOnLinkedList *StoreToMapBasedOnLinkedList(Company *c, struct MapBasedOnLinkedList *head){
	struct MapBasedOnLinkedList *n = malloc(sizeof(struct MapBasedOnLinkedList));
	n->company = c;
	n->next = head;
	n->key = c->symbol;
	return n;
} 

Company *FetchFromMapBasedOnLinkedList(struct MapBasedOnLinkedList *head, char *key, int numComp){
	struct MapBasedOnLinkedList *h = head;
	while (h != NULL){
		if (strcmp(h->key,key)  == 0){
			return h->company;
		}
		h = h->next;
	}
	return NULL;
}

void FetchAndPrintMapBasedOnLinkedList(struct MapBasedOnLinkedList *head, char *key, int n){
	struct MapBasedOnLinkedList *h = head;
	Company *c = FetchFromMapBasedOnLinkedList(h, key, n);
	if (c == NULL ){
		printf("There is no corresponding companies.\n");
		return;
	}
	PrintCompany(c);		
}

void InitializeMapBasedOnHashTable(MapBasedOnHashTable *map, int numElements)
{
    map->numElements = numElements;
    map->keys = malloc(sizeof(char *)*numElements);
    map->companies = malloc(sizeof(Company)*numElements);
    for (int i = 0 ; i < numElements ; i++)
        map->keys[i] = NULL;
}

void StoreTo_MapBasedOnHashTable(MapBasedOnHashTable *map, Company *c)
{
    int hashval = hash(c->symbol, map->numElements);
    for (int i = 0 ; i < map->numElements ; i++)
    {
        int idx = (hashval+i*(DoubleHash(hashval)))%(map->numElements);
        if (idx < 0) idx+= map->numElements;
        if (map->keys[idx] == NULL)
        {
            map->keys[idx]      = c->symbol;
            map->companies[idx] = *c;
            return;
        }
   }
}

Company *
FetchFrom_MapBasedOnHashTable(MapBasedOnHashTable *map, char *key)
{
    int hashval = hash(key, map->numElements);
    for (int i = 0 ; i < map->numElements ; i++)
    {
        int idx = (hashval+i*(DoubleHash(hashval)))%(map->numElements);
        if (idx < 0) idx+= map->numElements;
        if (map->keys[idx] == NULL)
        {
            return NULL;
        }

        if (strcmp(map->keys[idx], key) == 0)
        {
            return &(map->companies[idx]);
        }
}
    
    return NULL;
}

void FetchAndPrint(MapBasedOnHashTable *mfht, char *key)
{
    Company *c = FetchFrom_MapBasedOnHashTable(mfht, key);
    if (c == NULL)
    {
        printf("Key %s has no corresponding company\n", key);
    }
    else
    {
        PrintCompany(c);
    }

}

int main(int argc, char *argv[])
{
    Company *companies = NULL;
    int      numCompanies;
    ReadFile(argv[1], &companies, &numCompanies);
    struct MapBasedOnLinkedList *head = NULL; 
    MapBasedOnHashTable mfht;
    printf("num companies is %d\n", numCompanies);
    InitializeMapBasedOnHashTable(&mfht, numCompanies*2);
    gettimeofday(&tv1, NULL); 
    for (int i = 0 ; i < numCompanies ; i++)
    {
        StoreTo_MapBasedOnHashTable(&mfht, companies+i);
    }
    for (int i = 0; i < numCompanies ; i++){
        FetchFrom_MapBasedOnHashTable(&mfht, companies->symbol+i);
    }
    gettimeofday(&tv2, NULL);
 
    gettimeofday(&tv3, NULL);
    for (int i = 0; i < numCompanies; i++){
        head = StoreToMapBasedOnLinkedList(companies+i,head);
    }
    for (int i = 0; i < numCompanies ; i++){
       FetchFromMapBasedOnLinkedList(head, companies->symbol+i, numCompanies);
    }
    gettimeofday(&tv4, NULL);

    double HashTime = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 + (double) (tv2.tv_sec - tv1.tv_sec);
    double LinkedTime =  (double) (tv4.tv_usec - tv3.tv_usec) / 1000000 + (double) (tv4.tv_sec - tv3.tv_sec);
  

    printf("Total time for hash table to Store and Fetch all companies: %f seconds\n", HashTime);
    printf("Total time for linked list to Store and Fetch all companies: %f seconds\n", LinkedTime);

    if (HashTime < LinkedTime){
        printf("Yes Hash is faster\n");
    }
}

/* YOUR PERFORMANCE STUDY WRITEUP GOES HERE:
	I chose the Double Hash function that uses the return value 
	with prime - (key % prime) and the prime number being 7 because,
	the nature of prime numbers reduces the common factor between
	indicies which in turn would reduce the amount of collisions 
	while inserting values into the hashtable. Implementing the 
	linked list to achieve the same results as the hash table showed
	me that linked lists are not as efficient as I believed going
	into this project. The time shows that, the linked list took 
	20 times longer to Store and Fetch the 3400 companies from the 
	CSV file. Simply because you have to iterate through each node
	in the linked list to point to the next node which slows down
	the process immensley. 
 	 
 */
